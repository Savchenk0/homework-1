import React, { PureComponent } from "react";
import styles from "./Modal.module.scss";
import Button from "../Button/Button";
class Modal extends PureComponent{
    constructor(){
        super()
        this.state={
            show:true
        }
    }

    render(){
        const {header,closeButton,text,actions,isVisible,handleClick} = this.props  
        if (isVisible){ 
        return(
            <div>
                <div onClick={handleClick} className={styles.modalFull}>
                    <div onClick ={(e)=>e.stopPropagation()}className={styles.modalContent}>
                        <div className={styles.modalContentHeader}>
                            <p  className={styles.modalContentHeaderText}>{header}</p>
                            <Button  handleClick={handleClick} isShown={closeButton} text="X" className={styles.modalContentHeaderClose}/>
                        </div>
                        <div className={styles.modalContentBody}>
                                <p className={styles.modalContentBodyText}>{text}</p>
                                {actions()}    
                        </div>
                    </div>
                </div></div>
        )

    }   
        return null
        
    }

}

export default Modal