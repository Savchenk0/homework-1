import React, { PureComponent } from "react";
import styles from './Button.module.scss';

class Button extends PureComponent{
       
        render(){
        const {text,backgroundColor,handleClick,isShown = true} = this.props
        if (isShown){
            return (
            <button onClick ={handleClick}style={{backgroundColor:`${backgroundColor}`}}>{text}</button>
            )
        }
        return null
    }
        
}

export default Button