import './App.css';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import React, { PureComponent } from "react";
import btnStyles from './components/Modal/Modal.module.scss'


class App extends PureComponent {
  constructor(){
    super()
    this.state ={
      isModal1:false,
      isModal2:false
    }
  }
  render(){
   const {isModal1,isModal2} = this.state
  return (
    <div className="App">
              <Button handleClick={()=>this.setState(()=>{return{isModal1:true,isModal2:false}})}backgroundColor="green" text="Open first modal"/>
              <Button handleClick={()=>this.setState(()=>{return{isModal1:false,isModal2:true}})} backgroundColor="red" text="Open second modal"/> 

      <Modal handleClick={()=>this.setState(()=>({isModal1:false,isModal2:false}))}isVisible={isModal1} header="Do you want to delete this file?" closeButton={true} text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?" actions={()=>{return <div><a className={btnStyles.modalContentBodyBtn}>Ok</a>
                        <a className={btnStyles.modalContentBodyBtn}>Cancel</a></div>}}/>
      <Modal handleClick={()=>this.setState(()=>({isModal1:false,isModal2:false}))} isVisible={isModal2} header="Would you like to donate money?" closeButton={true} text="Once you've donated, it won't be possible to undo this action. Are you sure you want to donate?"actions={()=>{return <div><a className={btnStyles.modalContentBodyBtn}>plz dont donate</a>
                        <a className={btnStyles.modalContentBodyBtn}>plz donate</a></div>}}/>
    </div>
  );
    
  
     
       
       
    

  }
}

export default App;
